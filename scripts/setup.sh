#!/bin/bash

# Define the script name for logging
SCRIPT_NAME="install-audioshuffle"

# Ensure we are in the script directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
SERVICE_NAME="audioshuffle"
SERVICE_FILE="$DIR/../services/$SERVICE_NAME.service"
SYSTEMD_USER_DIR="$HOME/.config/systemd/user"
TARGET_SCRIPT="$DIR/audioshuffle.sh"

# Log helper function
log() {
    echo "[$SCRIPT_NAME] $1"
}

# Check if required files exist
if [[ ! -f "$TARGET_SCRIPT" ]]; then
    log "Error: Script '$TARGET_SCRIPT' not found. Exiting."
    exit 1
fi

if [[ ! -f "$SERVICE_FILE" ]]; then
    log "Error: Service file '$SERVICE_FILE' not found. Exiting."
    exit 1
fi

# Create systemd user directory if it doesn't exist
if [[ ! -d "$SYSTEMD_USER_DIR" ]]; then
    log "Creating systemd user directory: $SYSTEMD_USER_DIR"
    mkdir -p "$SYSTEMD_USER_DIR"
fi

# Copy the service file to the systemd user directory
log "Copying service file to $SYSTEMD_USER_DIR"
cp "$SERVICE_FILE" "$SYSTEMD_USER_DIR/$SERVICE_NAME.service"

# Update the service file's `ExecStart` path dynamically
log "Updating service file with correct script path"
sed -i "s|ExecStart=.*|ExecStart=$TARGET_SCRIPT|g" "$SYSTEMD_USER_DIR/$SERVICE_NAME.service"

# Reload systemd to apply the new service
log "Reloading systemd daemon for user"
systemctl --user daemon-reload

# Enable and start the service
log "Enabling and starting the $SERVICE_NAME service"
systemctl --user enable "$SERVICE_NAME"
systemctl --user restart "$SERVICE_NAME"

log "Installation complete. The '$SERVICE_NAME' service is now running."
